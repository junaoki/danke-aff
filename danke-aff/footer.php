<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish 
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>

</main><!-- / end page container, begun in the header -->

<footer class="site-footer">
	<div class="site-info container">
		
		<p>
			<a href="<?php echo home_url() ?>">トップページ</a>
		</p>
		<p>
			<a href="<?php echo home_url('/privacy_policy') ?>">プライバシーポリシー</a>
		</p>
		<p>
			<a href="<?php echo home_url('/term_of_service') ?>">利用規約</a>
		</p>
		<p>
			<a href="<?php echo home_url('/about_us') ?>">運営者情報</a>
		</p>
	</div><!-- .site-info -->
</footer><!-- #colophon .site-footer -->

<?php wp_footer(); 
// This fxn allows plugins to insert themselves/scripts/css/files (right here) into the footer of your website. 
// Removing this fxn call will disable all kinds of plugins. 
// Move it if you like, but keep it around.
?>

</body>
</html>

<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*  オリジナル
/*-----------------------------------------------------------------------------------*/
// 投稿記事のみを検索
function custom_search($search, $wp_query) {
    //サーチページ以外だったら終了
    if (!$wp_query->is_search) return;
    //投稿記事のみ検索
    $search .= " AND post_type = 'post'";
    return $search;
}
add_filter('posts_search','custom_search', 10, 2);
// カスタムヘッダー
add_theme_support( 'custom-header' );
// Define the version so we can easily replace it throughout the theme
define( 'NAKED_VERSION', 1.0 );

function self_made_post_type() {
    register_post_type( 'custom',
        array(
            'label' => 'まとめ', //表示名
            'public'        => true, //公開状態
            'exclude_from_search' => false, //検索対象に含めるか
            'show_ui' => true, //管理画面に表示するか
            'show_in_menu' => true, //管理画面のメニューに表示するか
            'menu_position' => 5, //管理メニューの表示位置を指定
            'hierarchical' => true, //階層構造を持たせるか
            'has_archive'   => false, //この投稿タイプのアーカイブを作成するか
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'custom-fields',
            ), //編集画面で使用するフィールド
        )
    );
}
add_action( 'init', 'self_made_post_type', 1 );

function add_whitelist_field($whitelist_options) {
    $whitelist_options['general'][] = 'star1';
    $whitelist_options['general'][] = 'star2';
    $whitelist_options['general'][] = 'star3';
    $whitelist_options['general'][] = 'star4';

    $whitelist_options['general'][] = 'free_text1';
    $whitelist_options['general'][] = 'free_text2';

    $whitelist_options['general'][] = 'rank1';
    $whitelist_options['general'][] = 'rank2';
    $whitelist_options['general'][] = 'rank3';
    $whitelist_options['general'][] = 'rank4';

    $whitelist_options['general'][] = 'order1';
    $whitelist_options['general'][] = 'order2';
    $whitelist_options['general'][] = 'order3';
    $whitelist_options['general'][] = 'order4';
    $whitelist_options['general'][] = 'order5';


    return $whitelist_options;
}
add_filter( 'whitelist_options', 'add_whitelist_field' );

function regist_custom_field() {
    add_settings_field( 'star_option', '★', 'display_star_option', 'general' );
    add_settings_field( 'free_text_option', '★の後ろのフリーテキスト', 'display_free_text_option', 'general');
    add_settings_field( 'rank_option', 'ランキング', 'display_rank_option', 'general');
    add_settings_field( 'order_option', '並び替え', 'display_order_option', 'general');
}
add_action( 'admin_init', 'regist_custom_field' );

function display_star_option() {
    $star1 = get_option( 'star1' );
    echo '<p>★1-1<input name="star1" id="star1" type="text" value="' . esc_html( $star1 ) .'" class="regular-text" /></p>';
    $star2 = get_option( 'star2' );
    echo '<p>★1-2<input name="star2" id="star2" type="text" value="' . esc_html( $star2 ) .'" class="regular-text" /></p>';
    $star3 = get_option( 'star3' );
    echo '<p>★2-1<input name="star3" id="star3" type="text" value="' . esc_html( $star3 ) .'" class="regular-text" /></p>';
    $star4 = get_option( 'star4' );
    echo '<p>★2-2<input name="star4" id="star4" type="text" value="' . esc_html( $star4 ) .'" class="regular-text" /></p>';
}
function display_free_text_option() {
    $free_text1 = get_option( 'free_text1' );
    echo '<p>text1-1<input name="free_text1" id="free_text1" type="text" value="' . esc_html( $free_text1 ) .'" class="regular-text" /></p>';
    $free_text2 = get_option( 'free_text2' );
    echo '<p>text1-2<input name="free_text2" id="free_text2" type="text" value="' . esc_html( $free_text2 ) .'" class="regular-text" /></p>';
}
function display_rank_option() {
    $rank1 = get_option( 'rank1' );
    echo '<p>rank1<input name="rank1" id="rank1" type="text" value="' . esc_html( $rank1 ) .'" class="regular-text" /></p>';
    $rank2 = get_option( 'rank2' );
    echo '<p>rank2<input name="rank2" id="rank2" type="text" value="' . esc_html( $rank2 ) .'" class="regular-text" /></p>';
    $rank3 = get_option( 'rank3' );
    echo '<p>rank3<input name="rank3" id="rank3" type="text" value="' . esc_html( $rank3 ) .'" class="regular-text" /></p>';
    $rank4 = get_option( 'rank4' );
    echo '<p>rank4<input name="rank4" id="rank4" type="text" value="' . esc_html( $rank4 ) .'" class="regular-text" /></p>';
}
function display_order_option() {
    $rank1 = get_option( 'order1' );
    echo '<p>rank1<input name="rank1" id="rank1" type="text" value="' . esc_html( $order1 ) .'" class="regular-text" /></p>';
    $rank2 = get_option( 'rank2' );
    echo '<p>rank2<input name="rank2" id="rank2" type="text" value="' . esc_html( $order2 ) .'" class="regular-text" /></p>';
    $rank3 = get_option( 'rank3' );
    echo '<p>rank3<input name="rank3" id="rank3" type="text" value="' . esc_html( $order3 ) .'" class="regular-text" /></p>';
    $rank4 = get_option( 'rank4' );
    echo '<p>rank4<input name="rank4" id="rank4" type="text" value="' . esc_html( $order4 ) .'" class="regular-text" /></p>';
}


/* テキストエリアを生成 */
function create_text_area($keyname){
    global $post;
    // 保存されているカスタムフィールドの値を取得
    $get_value = get_post_meta( $post->ID, $keyname, true );
    // nonceの追加
    wp_nonce_field( 'action-' . $keyname, 'nonce-' . $keyname );
    // HTMLの出力
    echo '<textarea name="' . $keyname . '">' . $get_value . '</textarea>';
}
/* 数値のラジオボタンを生成*/
/* $int_data = radioの値を配列で受ける*/
function create_radio_button_int($keyname, $int_data){
    global $post;
    // 保存されているカスタムフィールドの値を取得
    $get_value = intval(get_post_meta( $post->ID, $keyname, true ));
    // nonceの追加
    wp_nonce_field( 'action-' . $keyname, 'nonce-' . $keyname );
    // HTMLの出力
    foreach( $int_data as $d ) {
        $checked = '';
        if( $d === $get_value ) $checked = ' checked';
        echo '<label><input type="radio" name="' . $keyname . '" value="' . $d . '"' . $checked . '>' . $d . '</label>';
    }
}
/* 文字のラジオボタンを生成*/
/* $str_data = radioの値を配列で受ける*/
function create_radio_button_str($keyname, $str_data){
    global $post;
    $get_value = get_post_meta( $post->ID, $keyname, true );
    // nonceの追加
    wp_nonce_field( 'action-' . $keyname, 'nonce-' . $keyname );
    // HTMLの出力
    foreach( $str_data as $d ) {
        $checked = '';
        if( $d === $get_value ) $checked = ' checked';
        echo '<label><input type="radio" name="' . $keyname . '" value="' . $d . '"' . $checked . '>' . $d . '</label>';
    }
}
/* HTMLエディタを生成 */
function create_html_editor($keyname) {
    global $post;
    // 保存されているカスタムフィールドの値を取得
    $get_value = get_post_meta( $post->ID, $keyname, true );
    // nonceの追加
    wp_nonce_field( 'action-' . $keyname, 'nonce-' . $keyname );
    // HTMLの出力
    wp_editor( $get_value, $keyname . '-box', ['textarea_name' => $keyname] );
}

// カスタム投稿(まとめ)にカスタムフィールドの追加
// 固定カスタムフィールドボックス
function add_matome_fields() {
	add_meta_box( 'custom-matome-position', '設置場所', 'create_matome_position', 'custom', 'normal');
	add_meta_box( 'custom-matome-link', 'Link', 'create_matome_link', 'custom', 'normal');
    add_meta_box( 'custom-matome_link_attribute', 'LinkAttribute', 'create_matome_link_attribute', 'custom', 'normal' );
	add_meta_box( 'custom-matome-link_title', 'タイトル', 'create_matome_link_title', 'custom', 'normal');
}
add_action('admin_menu', 'add_matome_fields');



function create_matome_position() {
    create_radio_button_str('matome_position', ['header', 'footer']);
}

function create_matome_link() {
    create_text_area('matome_link');
}

function create_matome_link_attribute() {
    create_text_area('matome_link_attribute');
}

function create_matome_link_title() {
    create_text_area('matome_link_title');
}

// カスタムフィールドの保存
add_action( 'save_post', 'save_matome_field' );
function save_matome_field( $post_id ) {
    $custom_fields = ['matome_position', 'matome_link', 'matome_link_attribute','matome_link_title'];
    foreach( $custom_fields as $d ) {
        if ( isset( $_POST['nonce-' . $d] ) && $_POST['nonce-' . $d] ) {
            if( check_admin_referer( 'action-' . $d, 'nonce-' . $d ) ) {
                if( isset( $_POST[$d] ) && $_POST[$d] ) {
                    update_post_meta( $post_id, $d, $_POST[$d] );
                } else {
                    delete_post_meta( $post_id, $d, get_post_meta( $post_id, $d, true ) );
                }
            }
        }
    }
}

// 投稿にカスタムフィールドの追加
add_action( 'admin_menu', 'add_custom_field' );
function add_custom_field() {
    add_meta_box( 'custom-item_tokutyo', '特徴', 'create_item_tokutyo', 'post', 'normal' );
    add_meta_box( 'custom-item_link', 'Link', 'create_item_link', 'post', 'normal' );
    add_meta_box( 'custom-item_link_attribute', 'LinkAttribute', 'create_item_link_attribute', 'post', 'normal' );
	add_meta_box( 'custom-item_link_title', 'LinkTitle', 'create_item_link_title', 'post', 'normal' );
    $star1 = get_option('star1');
    if($star1){
        add_meta_box( 'custom-item_star1', $star1, 'create_item_star', 'post', 'normal', 'low', '1' );
    }
    $star2 = get_option('star2');
    if($star2){
        add_meta_box( 'custom-item_star2', $star2, 'create_item_star', 'post', 'normal', 'low', '2' );
    }
    $star3 = get_option('star3');
    if($star3){
        add_meta_box( 'custom-item_star3', $star3, 'create_item_star', 'post', 'normal', 'low', '3' );
    }
    $star4 = get_option('star4');
    if($star4){
        add_meta_box( 'custom-item_star4', $star4, 'create_item_star', 'post', 'normal', 'low', '4' );
    }
    $free_text1 = get_option('free_text1');
    if($free_text1){
        add_meta_box( 'custom-item_free_text1', $free_text1, 'create_item_free_text', 'post', 'normal', 'low', '1');
    }
    $free_text2 = get_option('free_text2');
    if($free_text2){
        add_meta_box( 'custom-item_free_text2', $free_text2, 'create_item_free_text', 'post', 'normal', 'low', '2');
    }
    // これは固定で問題なし
    add_meta_box( 'custom-item_rank_top', 'トップページ表示順', 'create_item_rank_top', 'post', 'normal' );

    $rank1 = get_option('rank1');
    if($rank1){
        add_meta_box( 'custom-item_rank1', $rank1, 'create_item_rank', 'post', 'normal', 'low', '1');
    }
    $rank2 = get_option('rank2');
    if($rank2){
        add_meta_box( 'custom-item_rank2', $rank2, 'create_item_rank', 'post', 'normal', 'low', '2');
    }
    $rank3 = get_option('rank3');
    if($rank3){
        add_meta_box( 'custom-item_rank3', $rank3, 'create_item_rank', 'post', 'normal', 'low', '3');
    }
    $rank4 = get_option('rank4');
    if($rank4){
        add_meta_box( 'custom-item_rank4', $rank4, 'create_item_rank', 'post', 'normal', 'low', '4');
    }
	add_meta_box( 'custom-item_order_sougou', '並び替え_総合評価', 'create_item_order_sougou', 'post', 'normal' );
	add_meta_box( 'custom-item_order_syurui', '並び替え_通貨種類', 'create_item_order_syurui', 'post', 'normal' );
}

function create_item_tokutyo() {
    create_html_editor('item_tokutyo');
}
function create_item_link() {
    create_text_area('item_link');
}
function create_item_link_attribute() {
    create_text_area('item_link_attribute');
}
function create_item_link_title() {
    create_text_area('item_link_title');
}
function create_item_star($post, $arguments){
    $key = 'item_star' . strval($arguments['args']);
    create_radio_button_int($key, [1,2,3,4,5,6,7,8,9,10]);
}

function create_item_free_text($post, $arguments){
    $key = 'item_free_text' . strval($arguments['args']);
    create_text_area($key);
}
function create_item_rank($post, $arguments){
    $key = 'item_rank' . strval($arguments['args']);
    create_radio_button_int($key, [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]);
}

function create_item_rank_top() {
    create_radio_button_int('item_rank_top', [1,2,3,4,5,10]);
}
function create_item_order_sougou() {
    create_radio_button_int('item_order_sougou', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]);
}

function create_item_order_syurui() {
    create_radio_button_int('item_order_syurui', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]);
}

// カスタムフィールドの保存
add_action( 'save_post', 'save_custom_field' );
function save_custom_field( $post_id ) {
    $custom_fields = ['item_tokutyo', 'item_link', 'item_link_attribute', 'item_link_title', 'item_star1', 'item_star2', 'item_star3', 'item_star4','item_free_text1', 'item_free_text2','item_rank_top', 'item_rank1', 'item_rank2', 'item_rank3', 'item_rank4', 'item_order_sougou', 'item_order_syurui'];
 
    foreach( $custom_fields as $d ) {
        if ( isset( $_POST['nonce-' . $d] ) && $_POST['nonce-' . $d] ) {
            if( check_admin_referer( 'action-' . $d, 'nonce-' . $d ) ) {
 
                if( isset( $_POST[$d] ) && $_POST[$d] ) {
                    update_post_meta( $post_id, $d, $_POST[$d] );
                } else {
                    delete_post_meta( $post_id, $d, get_post_meta( $post_id, $d, true ) );
                }
            }
        }
    }
}

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) $content_width = 900;

/*-----------------------------------------------------------------------------------*/
/* Add Rss feed support to Head section
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );

/*-----------------------------------------------------------------------------------*/
/* Add post thumbnail/featured image support
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus( 
	array(
		'primary'	=>	__( 'Primary Menu', 'naked' ), // Register the Primary menu
		// Copy and paste the line above right here if you want to make another menu, 
		// just change the 'primary' to another name
	)
);

/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function naked_register_sidebars() {
	register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'sidebar', 					// Make an ID
		'name' => 'Sidebar',				// Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<div>',	// What to display before each widget
		'after_widget' => '</div>',	// What to display following each widget
		'before_title' => '<h3 class="side-title">',	// What to display before each widget's title
		'after_title' => '</h3>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
		// Copy and paste the lines above right here if you want to make another sidebar, 
		// just change the values of id and name to another word/name
	));
} 
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'naked_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function naked_scripts()  { 

	// get the theme directory style.css and link to it in the header
	wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/style.css');
	
	// add fitvid
	wp_enqueue_script( 'naked-fitvid', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), NAKED_VERSION, true );
	
	// add theme scripts
	wp_enqueue_script( 'naked', get_template_directory_uri() . '/js/theme.min.js', array(), NAKED_VERSION, true );
  
}
add_action( 'wp_enqueue_scripts', 'naked_scripts' ); // Register this fxn and allow Wordpress to call it automatcally in the header

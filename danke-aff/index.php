<?php
get_header(); // This fxn gets the header.php file and renders it ?>
	<div id="primary" class="row-fluid">
		<?php get_search_form(); ?>
		<div id="content">
			<div class="title_frame">
				<h2 class="title">
					<?php bloginfo('name') ?>
				</h2>
			</div>
			<div class="post matome">
				<?php 
				// 登録されたまとめの最初の一件だけ表示
				$args = array(
					'numberposts' => 1,
					'post_type' => 'custom',
					'meta_key' => 'matome_position',
					'meta_value' => 'header'
				);
				// 
				$posts = get_posts( $args );
				// foreachだけど実際は1件しか帰らない
				if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
					<?php if ( ( function_exists('has_post_thumbnail') ) && ( has_post_thumbnail() ) ) { 
							$post_thumbnail_id = get_post_thumbnail_id();
							$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
							?>
							<div class="post-image">
								<a href="<?php echo $post->matome_link?>" <?php echo $post->matome_link_attribute ?> target="_blank">
									<img class="wp-post-image" src="<?php echo $post_thumbnail_url; ?>" >
								</a>			
							</div>
					<?php } ?>
					<div class="the-content">
						<div class="post_content">
							<?php the_content(); ?>
						</div>
					</div>
					<?php if($post->matome_link && $post->matome_link_title) { ?>
						<div class="post_button">
							<a href="<?php echo $post->matome_link?>" <?php echo $post->matome_link_attribute ?> target="_blank">
								<div class="button_to_oficial"><?php echo $post->matome_link_title ?></div>
							</a>
						</div>
					<?php } ?>
				<?php endforeach; ?>
				<?php else : //記事が無い場合 ?>
				<?php endif;
				wp_reset_postdata(); //クエリのリセット ?>
			</div>
			<?php
				// このページでトップページとランキングページを表現する
				$order = isset($_GET['orderby']) ? $_GET['orderby'] : null;
				// orderがあれば全件表示 なければトップページなので5件のみ
				$view_count = $order ? 100 : 5;
				// orderに値があればそれでソート、なければランキング順
				$order = $order ? 'item_' . $order : 'item_rank_top';
				$args = array(
				  'meta_key' => $order,
				  'orderby' => 'meta_value_num',
				  'order'    => 'ASC',
				  'posts_per_page' => $view_count
				);
				query_posts($args);
			?>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article>
						<div class="post">
							<h2 class="post_title">
								<a href="<?php echo $post->item_link ?>" <?php echo $post->item_link_attribute ?> title="<?php the_title(); ?>">
									<?php the_title();  ?>
								</a>
							</h2>
							<?php if ( ( function_exists('has_post_thumbnail') ) && ( has_post_thumbnail() ) ) { 
								$post_thumbnail_id = get_post_thumbnail_id();
								$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
								?>
								<div class="post-image">
									<a href="<?php echo $post->item_link ?>" <?php echo $post->item_link_attribute ?>>
										<img class="wp-post-image" src="<?php echo $post_thumbnail_url; ?>" >
									</a>
								</div>
							<?php } ?>
							<div class="the-content">
								<div class="post_tokutyo">
									<?php // 特徴
										echo $post->item_tokutyo ?>
								</div>
								<div class="post_button">
									<a href="<?php echo $post->item_link?>" <?php echo $post->item_link_attribute ?> target="_blank">
										<div class="button_to_oficial"><?php echo $post->item_link_title?></div>
									</a>
								</div>
								<div class="post_content">
									<?php the_content(); ?>
								</div>
							</div><!-- the-content -->
							<?php 
								$star1 = get_option( 'star1' );
								$star2 = get_option( 'star2' );
								$star3 = get_option( 'star3' );
								$star4 = get_option( 'star4' );
								$free_text1 = get_option( 'free_text1' );
							    $free_text2 = get_option( 'free_text2' );
							    
							    $star_array = array(1 => $star1, 2 => $star2, 3 => $star3, 4 => $star4);
							    $free_text_array = array(1 => $free_text1, 2 => $free_text2);
							    $filtered_star_array = array_filter($star_array, function($star) {
							    	return $star != null;
							    });
							    $filtered_free_text_array = array_filter($free_text_array, function($free_text) {
							    	return $free_text != null;
							    });
							?>
							<?php if(count($filtered_star_array) > 0 && count($filtered_free_text_array) > 0) { ?>
								<div class="post_rate"> 
									<table border="1" width="100%">
										<tbody>
											<?php if($star1 != null || $star2 != null){ ?>
												<tr>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star1 ?>
													</span></td>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star2 ?>
													</span></td>
												</tr>
												<tr>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star1 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star2 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
												</tr>
											<?php } ?>
											<?php if($star3 != null || $star4 != null){ ?>
												<tr>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star3 ?>
													</span></td>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star4 ?>
													</span></td>
												</tr>
												<tr>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star3 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star4 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
												</tr>
											<?php } ?>
											<?php if($free_text1 != null || $free_text2 != null){ ?>
												<tr><td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
													<?php echo $free_text1 ?>
												</span></td>
												<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
													<?php echo $free_text2 ?>
												</span></td>
												</tr>
												<tr><td width="50%"><div class="star-rating-box" align="center">
													<?php echo $post->item_freetext1?>
												</div></td>
												<td width="50%"><div class="star-rating-box" align="center">
													<?php echo $post->item_freetext2?>
												</div></td></tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							<?php } ?>
							<div class="post_button">
								<a href="<?php echo $post->item_link?>" <?php echo $post->item_link_attribute ?> target="_blank">
									<div class="button_to_oficial"><?php echo $post->item_link_title?></div>
								</a>
							</div>
						</div>
					</article>
				<?php endwhile; // OK, let's stop the posts loop once we've exhausted our query/number of posts ?>
			<?php else : // Well, if there are no posts to display and loop through, let's apologize to the reader (also your 404 error) ?>
				
				<article class="post error">
					<h1 class="404">Nothing has been posted like that yet</h1>
				</article>

			<?php endif; // OK, I think that takes care of both scenarios (having posts or not having any posts) ?>
			
			<div class="post matome">
				<?php 
				// 登録されたまとめの最初の一件だけ表示
				$args = array(
					'numberposts' => 1,
					'post_type' => 'custom',
					'meta_key' => 'matome_position',
					'meta_value' => 'footer'
				);
				// 
				$posts = get_posts( $args );
				// foreachだけど実際は1件しか帰らない
				if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
					<?php if ( ( function_exists('has_post_thumbnail') ) && ( has_post_thumbnail() ) ) { 
							$post_thumbnail_id = get_post_thumbnail_id();
							$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
							?>
							<div class="post-image">
								<a href="<?php echo $post->matome_link?>" <?php echo $post->matome_link_attribute ?> target="_blank">
									<img class="wp-post-image" src="<?php echo $post_thumbnail_url; ?>" >
								</a>			
							</div>
					<?php } ?>
					<div class="the-content">
						<div class="post_content">
							<?php the_content(); ?>
						</div>
					</div>
					<?php if($post->matome_link && $post->matome_link_title) { ?>
						<div class="post_button">
							<a href="<?php echo $post->matome_link?>" <?php echo $post->matome_link_attribute ?> target="_blank">
								<div class="button_to_oficial"><?php echo $post->matome_link_title ?></div>
							</a>
						</div>
					<?php } ?>
				<?php endforeach; ?>
				<?php else : //記事が無い場合 ?>
				<?php endif;
				wp_reset_postdata(); //クエリのリセット ?>
			</div>
			<section class="cnt_list rankings">
				<h2 class="title">ランキング</h2>
				<ul class="rankings_list">
					<li><a href="/?orderby=item_rank_one">総合ランキング</a></li>
					<?php
						$rank1 = get_option( 'rank1' );
						$rank2 = get_option( 'rank2' ); 
						$rank3 = get_option( 'rank3' ); 
						$rank4 = get_option( 'rank4' );
					?>
					<?php if($rank1){ ?>
						<li><a href="/?orderby=rank1"><?php echo $rank1 ?>ランキング</a></li>
					<?php } ?>
					<?php if($rank2){ ?>
						<li><a href="/?orderby=rank2"><?php echo $rank2 ?>ランキング</a></li>
					<?php } ?>
					<?php if($rank3){ ?>
						<li><a href="/?orderby=rank3"><?php echo $rank3 ?>ランキング</a></li>
					<?php } ?>
					<?php if($rank4){ ?>
						<li><a href="/?orderby=rank4"><?php echo $rank4 ?>ランキング</a></li>
					<?php } ?>
				</ul>
			</section>
		</div><!-- #content .site-content -->
	</div><!-- #primary .content-area -->
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
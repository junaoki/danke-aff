<?php
get_header(); // This fxn gets the header.php file and renders it ?>
<?php
	$categories = get_categories();
	$array = array();
	foreach($categories as $category) {
		$key = "sc_" . strval($category -> cat_ID);
		if (isset($_GET[$key])){
			$id_str = $_GET[$key];
			if($id_str) {
			 	array_push($array, intval($id_str));
			}
		}
		
	}
	$orderby = isset($_GET['orderby']) ? $_GET['orderby'] : 'item_order_sougou';

	$desc_val = isset($_GET['desc']) ? 'DESC' : 'ASC';
	$args = array(
		'category__and' => $array,
		'meta_key' => $orderby,
		'orderby' => 'meta_value_num',
		'order' => $desc_val
	);
	query_posts($args);
?>
	<div>
		<?php 
			// orderとdescを除いてquerystringを再構築
			$query_array = array();
			parse_str($_SERVER["REQUEST_URI"], $query_array);
			$new_query = '';
			foreach($query_array as $q => $r){
				if($q == 'orderby'){
					continue;
				}
				if($q == 'desc'){
					continue;
				}
				$new_query = $new_query . $q . '=' . $r . '&';
			}
		?>
		<div class="order-form">
			<select onChange="location.href=value;">
				<?php
					$orderby = $_GET['orderby'];
					$orderbydesc = $_GET['desc'] ? 1 : 0;
					$orderarray = array(
						new class { public $key = 'item_order_sougou'; public $title = '総合評価が高い'; public $desc = 0; },
						new class { public $key = 'item_order_syurui'; public $title = '通貨の種類が多い'; public $desc = 0; },
						new class { public $key = 'item_order_syurui'; public $title = '通貨の種類が少ない'; public $desc = 1; }
					);
					$selected_val = $orderby ? '' : 'selected';
					echo '<option value="' .$new_query . '" ' . $selected_val .'">並び替えを指定</option>';
					foreach($orderarray as $orderval) {
						
						echo $orderbydesc;
						echo $orderval -> desc;
						
						$selected_val = $orderby && $orderby == $orderval -> key && $orderbydesc == $orderval -> desc ? 'selected' : '';
						$desc = $orderval -> desc ? '&desc=1' : '';
						echo '<option value="' . $new_query . 'orderby=' . $orderval -> key . $desc . '" ' . $selected_val . '>' . $orderval -> title . '</option>' ;		
					}
				?>
			</select>	
		</div>
	</div>
	<div id="primary" class="row-fluid">
		<?php get_search_form(); ?>
		<div id="content" role="main">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article>
						<div class="post">
							<h2 class="post_title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<?php the_title();  ?>
								</a>
							</h2>
							<?php if ( ( function_exists('has_post_thumbnail') ) && ( has_post_thumbnail() ) ) { 
								$post_thumbnail_id = get_post_thumbnail_id();
								$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
								?>
								<div class="post-image">
									<a href="<?php echo $post->item_link ?>">
									<img class="wp-post-image" src="<?php echo $post_thumbnail_url; ?>">
									</a>
								</div>
							<?php } ?>
							<div class="the-content">
								<div class="post_tokutyo">
									<?php // 特徴
										echo $post->item_tokutyo ?>
								</div>
								<div class="post_button">
									<a href="<?php echo $post->item_link?>" target="_blank">
										<div class="button_to_oficial"><?php echo $post->item_link_title?></div>
									</a>
								</div>
								<div class="post_content">
									<?php the_content(); ?>
								</div>
							</div><!-- the-content -->
							<?php 
								$star1 = get_option( 'star1' );
								$star2 = get_option( 'star2' );
								$star3 = get_option( 'star3' );
								$star4 = get_option( 'star4' );
								$free_text1 = get_option( 'free_text1' );
							    $free_text2 = get_option( 'free_text2' );
							    
							    $star_array = array(1 => $star1, 2 => $star2, 3 => $star3, 4 => $star4);
							    $free_text_array = array(1 => $free_text1, 2 => $free_text2);
							    $filtered_star_array = array_filter($star_array, function($star) {
							    	return $star != null;
							    });
							    $filtered_free_text_array = array_filter($free_text_array, function($free_text) {
							    	return $free_text != null;
							    });
							?>
							<?php if(count($filtered_star_array) > 0 && count($filtered_free_text_array) > 0) { ?>
								<div class="post_rate"> 
									<table border="1" width="100%">
										<tbody>
											<?php if($star1 != null || $star2 != null){ ?>
												<tr>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star1 ?>
													</span></td>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star2 ?>
													</span></td>
												</tr>
												<tr>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star1 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star2 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
												</tr>
											<?php } ?>
											<?php if($star3 != null || $star4 != null){ ?>
												<tr>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star3 ?>
													</span></td>
													<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
														<?php echo $star4 ?>
													</span></td>
												</tr>
												<tr>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star3 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
													<td width="50%"><div class="star-rating-box" align="center"><div class="star-rating">
														<div class="star-rating-front" style="width: <?php echo $post->item_star4 * 10?>%;">★★★★★</div><div class="star-rating-back">★★★★★</div></div></div>
													</td>
												</tr>
											<?php } ?>
											<?php if($free_text1 != null || $free_text2 != null){ ?>
												<tr><td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
													<?php echo $free_text1 ?>
												</span></td>
												<td class="star_rating_header"><span style="margin: 2px; color: #ffffff; font-size: 14px; display: inline-block; width: 100%; text-align: center; font-weight: bold;">
													<?php echo $free_text2 ?>
												</span></td>
												</tr>
												<tr><td width="50%"><div class="star-rating-box" align="center">
													<?php echo $post->item_freetext1?>
												</div></td>
												<td width="50%"><div class="star-rating-box" align="center">
													<?php echo $post->item_freetext2?>
												</div></td></tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							<?php } ?>
							<div class="post_button">
								<a href="<?php echo $post->item_link?>" target="_blank">
									<div class="button_to_oficial"><?php echo $post->item_link_title?></div>
								</a>
							</div>
						</div>
					</article>
				<?php endwhile; // OK, let's stop the posts loop once we've exhausted our query/number of posts ?>
			<?php else : // Well, if there are no posts to display and loop through, let's apologize to the reader (also your 404 error) ?>
				
				<article class="post error">
					<h1 class="404">Nothing has been posted like that yet</h1>
				</article>
			<?php endif; // OK, I think that takes care of both scenarios (having posts or not having any posts) ?>
		</div><!-- #content .site-content -->
	</div><!-- #primary .content-area -->
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
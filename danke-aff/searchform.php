<section class="l-search">
	<div class="l-search-inner">
		<div class="l-search-box">
			<div class="l-search-title">
				<p id="plus001">おすすめ検索</p>
			</div>
			<div id="l-search-box-inner">
				<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					<?php
					//一番親階層のカテゴリをすべて取得
					//id順(登録順)
					$arg = array(
    					'parent' => 0,
						'orderby' => 'id'
					);
					$categories = get_categories($arg);
					foreach($categories as $val){
						//カテゴリのリンクURLを取得
						$cat_link = get_category_link($val->cat_ID);
						//カスタムフィールドでアイコン取得する用のIDを取得
						$post_id = 'category_' . $val -> cat_ID;
						//親カテゴリのリスト出力
						echo '<div class="flex_row">';
						echo '<span>' . $val -> name . '</span>';

						//子カテゴリのIDを配列で取得。配列の長さを変数に格納
						$child_cat_num = count(get_term_children($val->cat_ID,'category'));
						// checkするかの確認
						$has_value = isset($_GET['sc_' . $val -> cat_ID]) ? $_GET['sc_' . $val -> cat_ID] : null;
						//子カテゴリが存在する場合
						if($child_cat_num > 0){
							//子カテゴリの一覧取得条件
							$category_children_args = array(
								'parent' => $val->cat_ID,
								'orderby' => 'id'
							);
							//子カテゴリの一覧取得
							$category_children = get_categories($category_children_args);
							//子カテゴリの数だけリスト出力
							foreach($category_children as $child_val){
								$checked_val = "";
								if($has_value){
									if(intval($has_value) == $child_val -> cat_ID){
										$checked_val = 'checked="checked"';
									}
								}
								echo '<label class="s-01" for="sc_' . $child_val -> cat_ID . '">';
								echo '<input type="radio"' . $checked_val . 'class="radio-btn" id="sc_' . $child_val -> cat_ID . '" name="sc_' . $val -> cat_ID . '" value=' . $child_val -> cat_ID .' />' . $child_val -> name;
								echo '</label>';
							}
						}
						$checked_val = '';
						if(!$has_value){
							$checked_val = 'checked="checked"';
						}
						echo '<label class="s-01" for="sc_' . $val -> cat_ID . '">';
						echo '<input type="radio" id="sc_' . $val -> cat_ID . '" name="sc_' . $val -> cat_ID . '" value="" ' . $checked_val . '" />こだわりなし';
						echo '</div>';
					}
					?>
					<div class="flex_row flex_row_empty"></div>
					<div class="s-01-btn">
						<input type="submit" value="検索する">
					</div>
				<input type="hidden" name="s" id="s" />
			</form>
		</div>
	</div>
</section>
